import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {

  @ViewChild('carousel_scrollable', { static: true }) _carousel_scrollable!: ElementRef;

  ngOnInit() {
    this.total_cards = this._carousel_scrollable.nativeElement.querySelectorAll("app-carousel-card").length;
  }
  total_cards: number = 0;  // Calculated at startup
  focused_card: number = 0;

  /** Provides a value for the width of one card
   * In desktop mode, each card is only 500px while on mobile it is 70vw + 100px of margin and padding on each side
  */
  getScrollAmount(): number {
    console.log(window.innerWidth)
    if (window.innerWidth < 768) {
      return window.innerWidth * 0.7 + 100;
    } else {
      return 500;
    }
  }

  /**
   * Called when a scroll event occurs. Is responsible for updating the shaded position indicator
   */
  updateScroll() {
    this.focused_card = Math.round(this._carousel_scrollable.nativeElement["scrollLeft"] / this.getScrollAmount())
  }

  /**
   * Called when the user clicks on the arrow buttons to scroll view.
   * @param change Will represent the number of cards by which to move (-ve left +ve right)
   */
  clickScroll(change: number) {
    if (this.focused_card + change >= 0) { // Don't scroll beyond 0
      this._carousel_scrollable.nativeElement.scrollTo({
        left: (this.focused_card + change) * this.getScrollAmount(),
        behavior: 'smooth'
      })
    }

  }
}
