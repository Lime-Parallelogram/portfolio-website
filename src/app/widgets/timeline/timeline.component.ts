import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

export interface TimelineEntry {
  title: string,
  content: string,
  shown?: boolean,
};

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  @Input() myEntries: TimelineEntry[] = [];

  @ViewChild('timeline_main', { static: true }) _timeline_main!: ElementRef;

  MOBILE_CUTOFF = 450;

  ngOnInit() {
    this.updateScroll()  // Update currently shown cards based on initial screen size
  }

  updateScroll() {
    const target = this._timeline_main.nativeElement;
    let totalScrollPost = target["clientWidth"] + target['scrollLeft'];  // Represents furthest right point that is visible

    for (let i = 0; i < this.myEntries.length; i++) {  // Update isShown status for all cards
      if (target["clientWidth"] >= this.MOBILE_CUTOFF) {
        this.myEntries[i].shown = ((totalScrollPost >= (i+0.4) * 480)  // Show item on right once more than 40% of it is in view
                                   && !(target["scrollLeft"] >= (i+0.6) * 480));  // Hide item on left once more than 60% is off screen

      } else {  // On mobile, there is no fancy animation when the item goes out of view
        this.myEntries[i].shown = true;
      }
      
    }
  }

  /**
   * When a scroll event occurs
   */
  onScroll() {
    this.updateScroll()
  }

  /**
   * Called when left/right buttons are clicked. Moves timeline by specific amount
   * @param amount Number of pixels to move content by. Will likely be either 480px or -480px as this is the width of each timeline entry
   */
  clickScroll(amount: number) {
    this._timeline_main.nativeElement.scrollBy({
      left: amount,
      behavior: "smooth"
    })
  }
}
