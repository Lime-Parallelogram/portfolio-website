import { Component, Input } from '@angular/core';

export type linkDefinition = {
  icon: string,
  linkName: string,
  url: string,
}

@Component({
  selector: 'app-carousel-card',
  templateUrl: './carousel-card.component.html',
  styleUrls: ['./carousel-card.component.scss']
})
export class CarouselCardComponent {
  @Input() title = "";
  @Input() subtitle = "";
  @Input() image = "";
  @Input() image_alt = "";
  @Input() date = "";

  @Input() tags: Array<string> = [];

  @Input() links: Array<linkDefinition> = [];

}
