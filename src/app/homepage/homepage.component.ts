import { Component } from '@angular/core';

import {TimelineEntry} from '../widgets/timeline/timeline.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent {

  skills_entries: TimelineEntry[] = [{
    title: "Python",
    content: "Python was the first programming language I learned and it remains the language I am most comfortable in. Given the diversity of the Python language, it finds uses across a wide variety of applications; I have personally had extensive experience using it for hardware projects on Raspberry Pi, for building websites using Django or Flask and even just for scripting to automate tasks. I have also authored a publicly available library."
  },
  {
    title: "Web Development",
    content: "I have a strong understanding of web technologies; both in their vanilla form and when used with modern frameworks like Angular. My first project, the Pirate Game, was written in pure HTML, CSS and JS which really helped my understanding of the languages but also showed me first hand the value of using a modern framework for larger projects.",
  },
  {
    title: "Django",
    content: "I like the formal structure of Django projects and the way it forces you to separate functionalities into apps. I have extensive experience using the Django REST framework alongside an Angular front-end; having worked on the Pirate Game using a basic Flask server, the extra structure of REST Framework was a welcome change.",
  },
  {
    title: "Data Visualization",
    content: "I love collecting and studying data of all kinds. What really got me into this was starting to collect data about our home energy consumption via HomeAssistant. Since then I've worked with tools like Grafana to create graphs and dashboards using data from a range of different sources. See the 'York Energy Monitoring' project to see how I used this alongside data scraping and InfluxDB. I have also written more involved SQL queries to display data about my time use from Traggo or eating habits from Grocy.",
  },
  {
    title: "Linux Admin",
    content: "I have been involved in the Linux space since 2014 when I got my first Raspberry Pi. I have become very familiar with the command line over the years and I have experimented running a number of different server operating systems on pieces of old hardware at home. Currently I am running Proxmox, on top of which I have a wide range of distros running in containers and VMs. I have been using Linux as a daily driver on my laptop since 2019. "
  },
  {
    title: "Arduino & Electronics",
    content: "I have been into electronics and wires literally as long as I've been able to walk, there are pictures of 3-year old me spending hours playing with the circuits exhibit at Hendon Air Museum: it was my favorite thing there for years. Having worked with Raspberry Pi for a long time, I eventually started to get acquainted with Arduino as a may to minimize my projects, make them cheaper and more reliable."
  },
  {
    title: "Raspberry Pi",
    content: "I have an extensive catalogue of Raspberry Pi based projects. So far, I have mostly worked in Python with libraries like RPi.GPIO, sockets, pygame and MincraftPy among others. You will find a number or Pi-based robotics projects in the projects section."
  },
  {
    title: "Docker",
    content: "One of the technologies I have become increasingly familiar with as a result of my home server is Docker. These days, a large amount of my self-hosted services are managed with Docker and I have extensive experience packaging software into Docker images. I have published images associated with a number of the projects mentioned below."
  },
  {
    title: "Azure & VPS Hosting",
    content: "I love hosting things at home because it is both cheap and I know exactly where the data is but there are cases where this is not feasible. I manage a number of small VPS that are hosted with Fasthosts and as part of the Dialectic Union project, I have started working with more flexible cloud hosting options like Azure functions. I have also achieved the AZ-900 Azure certification."
  }];

  //^ Other Interests ^//
  other_interests_entries: TimelineEntry[] = [{
    title: "Renewables & Environment",
    content: "I've always been interested in nature and sustainability but my particular interest in renewable energy was piqued as the issue of energy generation was brought into the spotlight by the cost of living crisis. I am a big believer in the power of renewables and I remain optimistic that they will be at the forefront of the battle against climate change. I do know however that decarbonizing electricity only goes so far - we also need to reduce general consumption of 'stuff' everything from meats and electronics to general 'tat'."
  },
  {
    title: "Low-Carbon Technology",
    content: "In some ways I did things in reverse; I became interested in certain low-carbon technologies which then lead into my current views about the importance of sustainability and net zero. I absolutely LOVE heat pumps and everything that surrounds them, I think their importance in the decarbonization of domestic heating is almost non-negotiable. Similarly with EVs, I had a hand in encouraging my parents into their first EV relatively recently and I think their future dominance of motoring is becoming increasingly clear.",
  },
  {
    title: "Politics & Current Affairs",
    content: "As it turns out, quite a few things are influenced by politics. I think it is important to understand what is going on so you can make an informed use of your vote. I like to keep up with the news and am a regular listener to the Rest Is Politics podcast. I'm also a member of the York Global Affairs society, which often has guest speakers talking a range of lesser known happenings around the world. Honestly, being British, I do also love an opportunity to complain - and there's a buffet of things to complain about in politics.",
  },
  {
    title: "Cooking",
    content: "There's nothing better than serving a delicious, home-cooked meal to good company. Its a great distraction from whatever I've been working on to just pop some music on and focus on the kitchen. I always prioritize making nutritious, enjoyable food and am especially keen to make dishes that showcase how ingredients like tofu and plant-based meat substitutes can be used to best affect. Check my Mastodon feed as I occasionally post dishes I'm particularly impressed with.",
  },
  {
    title: "Free & Open Source Software",
    content: "I strongly believe in standards and interoperability. I think people should have the right to choose what software they use and shouldn't be held hostage to specific corporations by other people in their social circles. Users should have the freedom to interconnect and extend software to best suit them; a platform should work for its users. That, to me, is what fundamentally underpins the FOSS Philosophy. Equally, in many cases, just like with low-carbon tech, the FOSS solution is genuinely just better."
  }];

}
